package com.jj.latencyserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LatencyserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(LatencyserverApplication.class, args);
	}

}
