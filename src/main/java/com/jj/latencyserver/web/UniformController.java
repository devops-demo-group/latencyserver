package com.jj.latencyserver.web;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jj.latencyserver.domain.ResponseTime;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UniformController {

	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/uniform")
	@ResponseBody
	@Timed(histogram = true)
	public ResponseTime uniform(@RequestParam(name = "latency", required = true) long latency)
			throws InterruptedException {
		log.trace("UniformController.uniform");
		
		Thread.sleep(latency);

		return new ResponseTime(counter.incrementAndGet(), latency);
	}
}