package com.jj.latencyserver.web;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jj.latencyserver.domain.ResponseTime;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class GaussianController {

	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/gaussian")
	@ResponseBody
	@Timed(histogram = true)
	public ResponseTime gaussian(@RequestParam(name = "mean_latency", required = true) long mean_latency,
			@RequestParam(name = "sd_latency", required = true) long sd_latency) throws InterruptedException {
		
		log.trace("GaussianController.gaussian");
		
		final Random rnd = ThreadLocalRandom.current();
		long latency = 0;

		// be sure to not get latency smaller then 0
		do {
			latency = Math.round(rnd.nextGaussian() * sd_latency + mean_latency);
		} while (latency <= 0);

		Thread.sleep(latency);

		return new ResponseTime(counter.incrementAndGet(), latency);
	}
}
